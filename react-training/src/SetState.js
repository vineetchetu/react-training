import React from 'react';

class App extends React.Component {
   constructor() {
      super();
		
      this.state = {
         data: []
      }
	
      this.setStateHandler = this.setStateHandler.bind(this);
   };
   setStateHandler() {
      var item = "setState..."
      var myArray = this.state.data.slice();
	  myArray.push(item);
      this.setState({data: myArray})
	  
	  //this.state.data.push(item);
	  //this.forceUpdate()
   };
   render() {
	   console.log('data are '+this.state.data)
	   
      return (
         <div>
            <button onClick = {this.setStateHandler}>SET STATE</button>
            <h4>vineet :<span>{this.state.data}</span></h4>
			<h4>State Array : {this.state.data}</h4>
			<h4>Random number: {Math.random()}</h4>
         </div>
      );
   }
}
export default App;