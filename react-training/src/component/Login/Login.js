import React, { Component } from 'react';

class Login extends Component {
	constructor(props){
		super(props);
		console.log('child constructor');
	}
	
	componentWillMount(){
		console.log('child componentWillMount');
	}
	
	componentDidMount(){
		console.log('child componentDidMount');
	}
	
	componentWillReceiveProps(nextPropss){
		
		console.log('Child componentWillReceiveProps');
		//onsole.log(nextPropss);
	}
	
	shouldComponentUpdate(){
		console.log('child shouldComponentUpdate	');
		return true;
	}
	
	componentWillUpdate(nextProps, nextState){
		
		console.log('child componentWillUpdate	');
		console.log('next props',nextProps);
		console.log('next state', nextState);
	}
	
	componentDidUpdate(prevProps, prevState){
		
		console.log('child componentDidUpdate	');
		console.log('prev props',prevProps);
		console.log('prev state', prevState);
	}
	
	componentWillUnmount(){
		console.log('child component will unmount')
	}
	
  render() 
  {
	  console.log('child render');
	  
    return (
      <div>
		{this.props.name}
      </div>
    );
  }
}

export default Login;
