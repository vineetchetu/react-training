import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';

import Abc from './component/Login/Login'
import ForceUpdate from './ForceUpdate'
import SetState from './SetState'
import PrevState from './PrevState'

class App extends Component {
	constructor() {
        super()
        this.state = {           
			name:'Prem Verma'
        };
		console.log('constructor');
		
	
	}
	
	componentWillMount(){
		console.log('componentWillMount');
	}
	
	componentDidMount(){
		console.log('componentDidMount');
	}
	
	
	componentWillReceiveProps(nextProps){
		console.log('componentWillReceiveProps	');
	}
	
	shouldComponentUpdate(){
		console.log('shouldComponentUpdate	');
		this.forceUpdate();
		return false;		
	}
	
	componentWillUpdate(nextProps, nextState){
		console.log('componentWillUpdate	');
		console.log('next props',nextProps);
		console.log('next state', nextState);
	}
	
	componentDidUpdate(prevProps, prevState){
		
		console.log('componentDidUpdate	');
		console.log('prev props',prevProps);
		console.log('prev state', prevState);
	}
	
	
	componentWillUnmount(){
		console.log('component will unmount')
	}
	
	onclick(){
		this.setState({name: 'Ankit tomar'})
	}
	
	render() {
		console.log('render');
		
		
		return (
		  <div className="App"> 
		  |Parent Name : {this.state.name}   <br /><br />
		|Child Name : <Abc name={this.state.name} />
		
		<button onClick={this.onclick.bind(this)}> Change state </button>
			<ForceUpdate />
			<SetState />
			<PrevState />
		  </div>
		);
		
	}
}

export default App;
