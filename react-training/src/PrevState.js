import React, { Component } from 'react';

export default class PrevState extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isToggleOn: true,
	  isVineet: true
    }
    this.handleClick = this.handleClick.bind(this);
	this.handleClick1 = this.handleClick1.bind(this);
  }

  handleClick() {
    this.setState(prevState => {
      return { // we must return an object for setState
		isToggleOn: !prevState.isToggleOn
      } 
    });
  }
  
  handleClick1() {   
	this.setState({      
        isVineet: !this.state.isVineet     
    });
  }

  render() {
    return (
      <div>
        <button onClick={this.handleClick}>
          {this.state.isToggleOn ? 'ON' : "OFF"}
		</button>
		<br /><br />
		<button onClick={this.handleClick1}>
		{this.state.isVineet ? 'VINEET' : "SAINI"}
		</button>
      </div>
    );
  }
}