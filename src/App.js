import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Controlled from './components/Controlled'
//import Uncontrolled from './components/Uncontrolled'
//import Functional from './components/Functional'
import AddCounter from './components/AddCounter'
import Counter from './components/Counter'
import AppModal from './components/AppModal'
import Checkbox from './components/Checkbox'
import Autosuggest from './components/Autosuggest'
import Autosuggest2 from './components/Autosuggest2'
import MultiSelect from './components/MultiSelect/MultiSelect'

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React Project</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <Controlled />
        {/*<Uncontrolled />
        <Functional />*/}
        <AddCounter />
        <Counter />
        <AppModal buttonLabel="Click me" />
        <Checkbox />
		<Autosuggest />
		<Autosuggest2 />
		<MultiSelect />
      </div>
    );
  }
}

export default App;
