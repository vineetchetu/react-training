import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducer from './reducers';


const store = createStore(reducer);


ReactDOM.render(
		<Provider store={store}>
    		<App />
  		</Provider>, 
  		document.getElementById('root'));
registerServiceWorker();


// import React from 'react';
// import ReactDOM from 'react-dom';
// import './index.css';
// import App from './App';
// import registerServiceWorker from './registerServiceWorker';
// import Routes from './routes'

// //ReactDOM.render(<App />, document.getElementById('root'));
// ReactDOM.render(<Routes />, document.getElementById('root'));
// registerServiceWorker();


// import React from 'react';
// import ReactDOM from 'react-dom';
// import './index.css';
// import App from './App';
// import registerServiceWorker from './registerServiceWorker';
// //import Routes from './routes'
// import { BrowserRouter } from 'react-router-dom'

// ReactDOM.render((
// 	<BrowserRouter>
//     	<App />
// 	</BrowserRouter>), document.getElementById('root'));
// registerServiceWorker();
