import React, {Component} from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Home from '../components/Home'
import Controlled from '../components/Controlled'
import Uncontrolled from '../components/Uncontrolled'
import Functional from '../components/Functional'

const Routes = ()=>(
    <BrowserRouter>
        <Switch>
            <Route exact path='/' component={Home}/>
            <Route exact path='/controlled-components' component={Controlled}/>
            <Route exact path='/uncontrolled-components' component={Uncontrolled}/>
            <Route exact path='/functional-components' component={Functional}/>
        </Switch>
    </BrowserRouter>
)
export default Routes;