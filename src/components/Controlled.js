import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Controlled extends Component { 
  constructor(){
    super()
    this.state = { address:''}
  }
  
  onClick = () =>{    
    var add = this.state.address;
    alert(add)
  }

  onChange = (event) =>{
    this.setState({address:event.target.value})
  }

  render() {
    return (
      <div className="App">       
          <h1 className="App-title">Controlled Component</h1>
          <input type='text' value={this.state.address} onChange={this.onChange} />
          <input type="button" onClick={this.onClick} value="Save"/>
          <br /><br />
          {/*<Link to="/">Back to Home</Link>*/}
      </div>
    );
  }
}

//export default Home;
