import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import "./example.css";

const FLAVOURS = [
	{ label: 'Chocolate', value: '1' },
	{ label: 'Vanilla', value: '2' },
	{ label: 'Strawberry', value: '3' },
	{ label: 'Caramel', value: '4' },
	{ label: 'Cookies and Cream', value: '5' },
	{ label: 'Peppermint', value: '6' },
];

const WHY_WOULD_YOU = [
	{ label: 'Chocolate (are you crazy?)', value: 'chocolate', disabled: true },
].concat(FLAVOURS.slice(1));

export default class MultiSelectField extends Component{

	constructor(props) {
        super(props);
		this.state = {
			removeSelected: true,
			disabled: false,
			crazy: false,
			stayOpen: false,
			value: [],
			rtl: false,
		}
		this.handleSelectChange = this.handleSelectChange.bind(this)
		this.toggleCheckbox = this.toggleCheckbox.bind(this)
		this.toggleRtl = this.toggleRtl.bind(this)
	}
     
	handleSelectChange (value) {
		console.log('You\'ve selected:', value);
		this.setState({ value });
	}
	
	toggleCheckbox (e) {
		this.setState({
			[e.target.name]: e.target.checked,
		});
	}
	
	toggleRtl (e) {
		let rtl = e.target.checked;
		this.setState({ rtl });
	}
	
	render () {
		const { crazy, disabled, stayOpen, value } = this.state;
		const options = crazy ? WHY_WOULD_YOU : FLAVOURS;
		return (
			<div className="section">
				<h3 className="section-heading">{this.props.label}</h3>
				<Select
					closeOnSelect={!stayOpen}
					disabled={disabled}
					multi
					onChange={this.handleSelectChange}
					options={options}
					placeholder="Select your favourite(s)"
					removeSelected={this.state.removeSelected}
					//rtl={this.state.rtl}
					simpleValue
					value={value}
				/>				
			</div>
		);
	}
};