import React, { Component } from 'react';
import { Link, NavLink} from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Welcome to Home Page</h1>
        </header>  
        <nav>
        	<li><NavLink to="/controlled-components" >Controlled Components</NavLink></li>
        	<li><NavLink to="/uncontrolled-components" >Uncontrolled Components</NavLink></li>
        	<li><NavLink to="/functional-components" >Functional Components</NavLink></li>
    	</nav>          
      </div>
    );
  }
}

export default App;
