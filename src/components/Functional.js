/*
import React from 'react';

const Functional = () => (
	<div>
		<h1 className="App-title">Functional Component</h1>		
	</div>
);

export default Functional;
*/

import React from 'react';
import { Link } from 'react-router-dom';

export default () => (
	<div>
		<h1 className="App-title">Functional Component</h1>	
		<br /><br />
          <Link to="/">Back to Home</Link>	
	</div>
);
