import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Uncontrolled extends Component { 
  
  onClick = () =>{    
    var add = this.refs.address.value;
    alert(add)
    alert(this.input.value)
  }

  render() {
    return (
      <div className="App">       
          <h1 className="App-title">Uncontrolled Component</h1>
          <input type='text' ref="address" />
          <input type="text" ref={(input) => this.input = input} />
          <input type="button" onClick={this.onClick} value="Save"/>
          <br /><br />
          <Link to="/">Back to Home</Link>
      </div>
    );
  }
}

//export default Home;
