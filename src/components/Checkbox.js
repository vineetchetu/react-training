import React from 'react'
class Checkbox extends React.Component{    
    checkAllCheckbox(event) {
        const allCheckboxChecked = event.target.checked   
        var checkboxes = document.getElementsByName('subscription[]')        
        if(allCheckboxChecked){   
            for(var i in checkboxes){
                if(checkboxes[i].checked == false){
                    checkboxes[i].checked = true;
                }                
            }                    
        } else{
            for(var i in checkboxes){
                if(checkboxes[i].checked == true){
                    checkboxes[i].checked = false;
                }
            }           
        }                  
      }       
    render() {        
        return (                  
            <div className="container">                                   
                <div className="form-check form-checkbox">
                    <input type="checkbox" onChange={this.checkAllCheckbox} /> Select All 
                </div>
                <div>         
                    <input type="checkbox" name="subscription[]" /><br />
                    <input type="checkbox" name="subscription[]" /><br />
                    <input type="checkbox" name="subscription[]" /><br />                   
                </div>                            
            </div>)
        }
}
export default Checkbox